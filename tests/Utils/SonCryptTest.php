<?php

namespace App\Tests\Utils;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Utils\SonCrypt;

class SonCryptTest extends KernelTestCase
{
    /**
     * @var SonCrypt
     */
    private static $sonCrypt;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        self::$sonCrypt = self::$container->get('App\Utils\SonCrypt');
    }

    public function testMainInteraction()
    {
        $plain = 'this is a plain string';
        $encoded = self::$sonCrypt->encode($plain);

        $this->assertEquals($plain, self::$sonCrypt->decode($encoded));
    }

    public function testEmpty()
    {
        $emptyString = '';
        $encoded = self::$sonCrypt->encode($emptyString);

        $this->assertEquals($emptyString, self::$sonCrypt->decode($encoded));
    }

    public function testErrorInvalidInput()
    {
        $plain = 'notencodedpassword';
        $encoded = $plain;
        $this->assertNotEquals($plain, self::$sonCrypt->decode($encoded));
        $this->assertEquals(null, self::$sonCrypt->decode($encoded));
    }
}
