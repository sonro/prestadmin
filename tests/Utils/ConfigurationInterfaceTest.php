<?php

namespace App\Tests\Utils;

use App\Utils\ConfigurationInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ConfigurationInterfaceTest extends KernelTestCase
{
    /**
     * @var ConfigurationInterface
     */
    private $configurationInterface;

    public function setUp()
    {
        self::bootKernel();
        $this->configurationInterface = self::$container->get('App\Utils\ConfigurationInterface');
    }

    public function testMainInteraction()
    {
        $key = 'atestingkey';
        $value = 'atestingvalue';

        $this->configurationInterface->set($key, $value);

        $this->assertEquals($value, $this->configurationInterface->get($key));
    }
}
