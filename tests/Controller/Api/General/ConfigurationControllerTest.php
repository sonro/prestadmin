<?php

namespace App\Tests\Controller\Api;

use App\Tests\BaseFunctionalTestCase;
use App\Utils\ConfigurationInterface;

class ConfigurationControllerTest extends BaseFunctionalTestCase
{
    /**
     * @var ConfigurationInterface
     */
    private static $config;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$config = self::$container->get('App\Utils\ConfigurationInterface');
    }

    public function testShow()
    {
        $name = 'atestname';
        $value = 'atestvalue';
        self::$config->set($name, $value);

        $response = $this->getRequest('/api/configuration/'.$name);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($value, $data);
    }

    public function testShowUnknownKey()
    {
        $name = 'thisisnotavalueintheconfigurationdatabase';
        $response = $this->getRequest('/api/configuration/'.$name);
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testList()
    {
        $response = $this->getRequest('/api/configuration');
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(count($data) >= 2);
    }

    public function testPost()
    {
        $name = 'thisisnotyetavalueintheconfiguraitondatabase';
        $value = 'value';
        $data = [
            $name => $value,
        ];
        $json = json_encode($data);

        $response = $this->postRequest('/api/configuration', $json);
        $this->assertEquals(204, $response->getStatusCode());

        $response = $this->getRequest('/api/configuration/'.$name);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($value, $data);
    }

    public function testPostMultiple()
    {
        $name1 = 'newValue1';
        $value1 = 'value1';
        $name2 = 'newValue2';
        $value2 = 'value2';
        $data = [
            $name1 => $value1,
            $name2 => $value2,
        ];
        $json = json_encode($data);

        $response = $this->postRequest('/api/configuration', $json);
        $this->assertEquals(204, $response->getStatusCode());

        $response = $this->getRequest('/api/configuration/'.$name1);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($value1, $data);

        $response = $this->getRequest('/api/configuration/'.$name2);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($value2, $data);
    }

    public function testPut()
    {
        $name = 'testName1';
        $value = 'newTestValue';

        $json = json_encode($value);
        $uri = '/api/configuration/'.$name;

        $response = $this->putRequest($uri, $json);
        $this->assertEquals(204, $response->getStatusCode());

        $response = $this->getRequest($uri);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($value, $data);
    }
}
