<?php

namespace App\Tests\Controller\Api\Resource;

use App\Tests\BaseFunctionalTestCase;
use App\Entity\DbUser;
use App\Utils\SonCrypt;

class DbUserControllerTest extends BaseFunctionalTestCase
{
    /**
     * @var SonCrypt
     */
    private static $sonCrypt;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$sonCrypt = self::$container->get('App\Utils\SonCrypt');
    }

    public function testList()
    {
        $response = $this->getRequest('/api/resource/dbuser');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));

        $data = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('dbUsers', $data);
        $this->assertEquals(false, array_key_exists('password', $data['dbUsers'][0]));
    }

    public function testListWithPassword()
    {
        $response = $this->getRequest('/api/resource/dbuser', [
            'show-password' => null,
        ]);

        $data = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('password', $data['dbUsers'][0]);
    }

    public function testShow()
    {
        $dbUser = new DbUser();
        $dbUser->setUsername('ftest2-username');
        $password = 'ftest2-password';
        $dbUser->setPassword(self::$sonCrypt->encode($password));
        $dbUser->setDbHost('127.0.0.1');

        self::$entityManager->persist($dbUser);
        self::$entityManager->flush();

        $id = $dbUser->getId();
        $response = $this->getRequest("/api/resource/dbuser/$id");

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($dbUser->getUsername(), $data['username']);
    }

    public function testAdd()
    {
        $postData = [
            'username' => 'ftest-username',
            'password' => 'ftest-password',
        ];
        $json = json_encode($postData);

        $response = $this->postRequest('/api/resource/dbuser', $json);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue($response->headers->has('Location'));

        $location = $response->headers->get('Location');
        $showResponse = $this->getRequest($location);

        $showData = json_decode($showResponse->getContent(), true);
        $this->assertEquals($postData['username'], $showData['username']);
        $this->assertEquals('127.0.0.1', $showData['dbHost']);
    }

    public function testPUT()
    {
        $dbUser = new DbUser();
        $dbUser->setUsername('ftest-username');
        $password = 'ftest-password';
        $dbUser->setPassword(self::$sonCrypt->encode($password));
        $dbUser->setDbHost('127.0.0.1');

        self::$entityManager->persist($dbUser);
        self::$entityManager->flush();
        $id = $dbUser->getId();

        $putData = [
            'username' => 'ftest-putChangedUsername',
            'password' => $password,
            'dbHost' => '127.0.1.1',
        ];

        $json = json_encode($putData);
        $response = $this->putRequest("/api/resource/dbuser/$id", $json);

        $this->assertEquals(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals($password, $responseData['password']);
        $this->assertEquals($putData['username'], $responseData['username']);
        $this->assertEquals($putData['dbHost'], $responseData['dbHost']);
    }

    public function testPATCH()
    {
        $dbUser = new DbUser();
        $dbUser->setUsername('ftest-username');
        $password = 'ftest-password';
        $dbUser->setPassword(self::$sonCrypt->encode($password));
        $dbUser->setDbHost('127.0.0.1');

        self::$entityManager->persist($dbUser);
        self::$entityManager->flush();
        $id = $dbUser->getId();

        $patchData = [
            'dbHost' => '127.0.1.1',
        ];

        $json = json_encode($patchData);
        $response = $this->patchRequest("/api/resource/dbuser/$id", $json);

        $this->assertEquals(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals($password, $responseData['password']);
        $this->assertEquals($dbUser->getUsername(), $responseData['username']);
        $this->assertNotEquals($dbUser->getDbHost(), $responseData['dbHost']);
        $this->assertEquals($patchData['dbHost'], $responseData['dbHost']);
    }

    public function testDELETE()
    {
        $dbUser = new DbUser();
        $dbUser->setUsername('ftest-username');
        $dbUser->setPassword('ftest-password');
        $dbUser->setDbHost('127.0.0.1');

        self::$entityManager->persist($dbUser);
        self::$entityManager->flush();
        $id = $dbUser->getId();

        $response = $this->deleteRequest("/api/resource/dbuser/$id");

        $this->assertEquals(204, $response->getStatusCode());
    }
}
