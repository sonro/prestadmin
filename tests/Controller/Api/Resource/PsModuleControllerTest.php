<?php

namespace App\Tests\Controller\Api\Resource;

use App\Tests\BaseFunctionalTestCase;
use App\Entity\PsModule;

class PsModuleControllerTest extends BaseFunctionalTestCase
{
    public function testList()
    {
        $response = $this->getRequest('/api/resource/psmodule');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));

        $data = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('psModules', $data);
        $this->assertTrue(array_key_exists('apiUrl', $data['psModules'][0]));
    }

    public function testShow()
    {
        /** *@var PsModule */
        $psModule = self::$entityManager->getRepository(PsModule::class)->find(1);

        $id = $psModule->getId();
        $response = $this->getRequest("/api/resource/psmodule/$id");

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($psModule->getApiUrl(), $data['apiUrl']);
    }

    public function testPOST()
    {
        $postData = [
            'name' => 'ftest-moduleName',
            'apiUrl' => 'ftest-apiUrl',
            'psInstalls' => [1, 2, 3],
        ];
        $json = json_encode($postData);

        $response = $this->postRequest('/api/resource/psmodule', $json);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue($response->headers->has('Location'));

        $location = $response->headers->get('Location');
        $showResponse = $this->getRequest($location);
        $this->assertEquals(200, $showResponse->getStatusCode());

        $showData = json_decode($showResponse->getContent(), true);
        $this->assertEquals($postData['name'], $showData['name']);
    }

    public function testPUT()
    {
        $postData = [
            'name' => 'ftest-moduleName',
            'apiUrl' => 'ftest-putOriginalApiUrl',
            'psInstalls' => [1, 2, 3],
        ];
        $json = json_encode($postData);

        $response = $this->postRequest('/api/resource/psmodule', $json);
        $data = json_decode($response->getContent(), true);
        $id = $data['id'];

        $putData = [
            'name' => 'ftest-putChangedName',
            'apiUrl' => 'ftest-putOriginalApiUrl',
            'psInstalls' => [1, 2, 3],
        ];

        $json = json_encode($putData);
        $response = $this->putRequest("/api/resource/psmodule/$id", $json);

        $this->assertEquals(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(3, count($responseData['psInstalls']));
        $this->assertNotEquals($postData['name'], $responseData['name']);
        $this->assertEquals($putData['name'], $responseData['name']);
        $this->assertEquals($postData['apiUrl'], $responseData['apiUrl']);
    }

    public function testPATCH()
    {
        $postData = [
            'name' => 'ftest-moduleName',
            'apiUrl' => 'ftest-apiUrl',
            'psInstalls' => [1, 2, 3],
        ];
        $json = json_encode($postData);

        $response = $this->postRequest('/api/resource/psmodule', $json);
        $data = json_decode($response->getContent(), true);
        $id = $data['id'];

        $patchData = [
            'apiUrl' => 'ftest-patchChangedApiUrl',
        ];
        $json = json_encode($patchData);

        $patchResponse = $this->patchRequest("/api/resource/psmodule/$id", $json);

        $this->assertEquals(200, $patchResponse->getStatusCode());
        $responseData = json_decode($patchResponse->getContent(), true);
        $this->assertNotEquals($postData['apiUrl'], $responseData['apiUrl']);
        $this->assertEquals($patchData['apiUrl'], $responseData['apiUrl']);
    }

    public function testDELETE()
    {
        $postData = [
            'name' => 'ftest-moduleName',
            'apiUrl' => 'ftest-apiUrl',
            'psInstalls' => [1, 2, 3],
        ];
        $json = json_encode($postData);

        $response = $this->postRequest('/api/resource/psmodule', $json);
        $data = json_decode($response->getContent(), true);
        $id = $data['id'];

        $response = $this->deleteRequest("/api/resource/psmodule/$id");
        $this->assertEquals(204, $response->getStatusCode());

        $showResponse = $this->getRequest("/api/resource/psmodule/$id");
        $this->assertEquals(404, $showResponse->getStatusCode());
    }
}
