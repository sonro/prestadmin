<?php

namespace App\Tests\Controller\Api\Resource;

use App\Tests\BaseFunctionalTestCase;
use App\Entity\Employee;

class EmployeeControllerTest extends BaseFunctionalTestCase
{
    public function testList()
    {
        $response = $this->getRequest('/api/resource/employee');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));

        $data = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('employees', $data);
        $this->assertEquals(false, array_key_exists('password', $data['employees'][0]));
    }

    public function testShow()
    {
        $employee = new Employee();
        $employee->setEmail('thisis@testemail.org');
        $employee->setFirstname('ThisIsATestFirstName');
        $employee->setLastname('ThisIsATestLastName');
        $employee->setPassword('rubbishPassword');
        self::$entityManager->persist($employee);
        self::$entityManager->flush();
        $id = $employee->getId();

        $response = $this->getRequest('/api/resource/employee/'.$id);

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($employee->getFirstname(), $data['firstname']);
        $this->assertFalse(array_key_exists('password', $data));
    }

    public function testAdd()
    {
        $employeeData = [
            'email' => 'thisisalsoa@testemail.org',
            'firstname' => 'ThisIsATestFirstName',
            'lastname' => 'ThisIsATestLastName',
            'password' => 'rubbishPassword',
        ];

        $postJson = json_encode($employeeData);
        $response = $this->postRequest('/api/resource/employee', $postJson);
        $this->assertEquals(201, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertTrue($response->headers->has('Location'));
        $this->assertEquals(
            '/api/resource/employee/'.$data['id'],
            $response->headers->get('Location'));
    }

    public function testPut()
    {
        $employee = new Employee();
        $employee->setEmail('thisistheoriginal@testemail.org');
        $employee->setFirstname('ThisIsATestFirstName-Original');
        $employee->setLastname('ThisIsATestLastName-Original');
        $employee->setPassword('rubbishPassword');
        self::$entityManager->persist($employee);
        self::$entityManager->flush();
        $id = $employee->getId();

        $employeeData = [
            'email' => $employee->getEmail(),
            'firstname' => 'ThisIsATestFirstName',
            'lastname' => $employee->getLastname(),
            'password' => 'rubbishPassword',
        ];

        $json = json_encode($employeeData);
        $response = $this->putRequest('/api/resource/employee/'.$id, $json);
        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($employee->getLastname(), $data['lastname']);
        $this->assertNotEquals($employee->getFirstname(), $data['firstname']);
        $this->assertEquals($employeeData['firstname'], $data['firstname']);
    }

    public function testPatch()
    {
        $employee = new Employee();
        $employee->setEmail('thisistheoriginalpatch@testemail.org');
        $employee->setFirstname('ThisIsATestFirstName-Original');
        $employee->setLastname('ThisIsATestLastName-Original');
        $employee->setPassword('rubbishPassword');
        self::$entityManager->persist($employee);
        self::$entityManager->flush();
        $id = $employee->getId();

        $employeeData = [
            'firstname' => 'ThisIsATestFirstName',
        ];

        $json = json_encode($employeeData);
        $response = $this->patchRequest('/api/resource/employee/'.$id, $json);
        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertEquals($employee->getLastname(), $data['lastname']);
        $this->assertNotEquals($employee->getFirstname(), $data['firstname']);
        $this->assertEquals($employeeData['firstname'], $data['firstname']);
    }

    public function testDelete()
    {
        $employee = new Employee();
        $employee->setEmail('thisisthedelete@testemail.org');
        $employee->setFirstname('ThisIsATestFirstName-Original');
        $employee->setLastname('ThisIsATestLastName-Original');
        $employee->setPassword('rubbishPassword');
        self::$entityManager->persist($employee);
        self::$entityManager->flush();
        $id = $employee->getId();

        $response = $this->deleteRequest("/api/resource/employee/$id");
        $this->assertEquals(204, $response->getStatusCode());

        $showResponse = $this->getRequest("/api/resource/employee/$id");
        $this->assertEquals(404, $showResponse->getStatusCode());
    }

    public function testShowError()
    {
        $showResponse = $this->getRequest('/api/resource/employee/1001001001');
        $this->assertEquals(404, $showResponse->getStatusCode());
    }
}
