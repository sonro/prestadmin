<?php

namespace App\Repository;

use App\Entity\PsModuleRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PsModuleRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsModuleRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsModuleRequest[]    findAll()
 * @method PsModuleRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsModuleRequestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PsModuleRequest::class);
    }

    // /**
    //  * @return PsModuleRequest[] Returns an array of PsModuleRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PsModuleRequest
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
