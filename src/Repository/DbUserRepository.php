<?php

namespace App\Repository;

use App\Entity\DbUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DbUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method DbUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method DbUser[]    findAll()
 * @method DbUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DbUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DbUser::class);
    }

    // /**
    //  * @return DbUser[] Returns an array of DbUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DbUser
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
