<?php

namespace App\Repository;

use App\Entity\PsInstall;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PsInstall|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsInstall|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsInstall[]    findAll()
 * @method PsInstall[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsInstallRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PsInstall::class);
    }

    // /**
    //  * @return PsInstall[] Returns an array of PsInstall objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PsInstall
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
