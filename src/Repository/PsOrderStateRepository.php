<?php

namespace App\Repository;

use App\Entity\PsOrderState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PsOrderState|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsOrderState|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsOrderState[]    findAll()
 * @method PsOrderState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsOrderStateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PsOrderState::class);
    }

    // /**
    //  * @return PsOrderState[] Returns an array of PsOrderState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PsOrderState
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
