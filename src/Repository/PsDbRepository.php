<?php

namespace App\Repository;

use App\Entity\PsDb;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PsDb|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsDb|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsDb[]    findAll()
 * @method PsDb[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsDbRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PsDb::class);
    }

    // /**
    //  * @return PsDb[] Returns an array of PsDb objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PsDb
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
