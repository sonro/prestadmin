<?php

namespace App\Repository;

use App\Entity\PsShop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PsShop|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsShop|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsShop[]    findAll()
 * @method PsShop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsShopRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PsShop::class);
    }

    // /**
    //  * @return PsShop[] Returns an array of PsShop objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PsShop
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
