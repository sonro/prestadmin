<?php

namespace App\Repository;

use App\Entity\PsModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PsModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsModule[]    findAll()
 * @method PsModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsModuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PsModule::class);
    }

    // /**
    //  * @return PsModule[] Returns an array of PsModule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PsModule
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
