<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PsOrderStateRepository")
 * @JMS\ExclusionPolicy("all")
 * @JMS\AccessorOrder("alphabetical")
 */
class PsOrderState
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderState", inversedBy="psOrderStates")
     * @Assert\NotBlank()
     */
    private $orderState;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $psId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PsInstall", inversedBy="psOrderStates")
     * @Assert\NotBlank()
     */
    private $psInstall;

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("psInstall")
     */
    public function getPsIntallId()
    {
        return $this->psInstall->getId();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("orderState")
     */
    public function getOrderStateId()
    {
        return $this->psInstall->getId();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderState(): ?OrderState
    {
        return $this->orderState;
    }

    public function setOrderState(?OrderState $orderState): self
    {
        $this->orderState = $orderState;

        return $this;
    }

    public function getPsId(): ?int
    {
        return $this->psId;
    }

    public function setPsId(int $psId): self
    {
        $this->psId = $psId;

        return $this;
    }

    public function getPsInstall(): ?PsInstall
    {
        return $this->psInstall;
    }

    public function setPsInstall(?PsInstall $psInstall): self
    {
        $this->psInstall = $psInstall;

        return $this;
    }
}
