<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use App\Serialization\PsInstallIdsTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PsModuleRepository")
 * @JMS\ExclusionPolicy("all")
 */
class PsModule
{
    use PsInstallIdsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $apiUrl;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PsInstall", inversedBy="psModules")
     */
    private $psInstalls;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PsModuleRequest", mappedBy="psModule")
     * @JMS\Expose()
     */
    private $psModuleRequests;

    public function __construct()
    {
        $this->psInstalls = new ArrayCollection();
        $this->psModuleRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getApiUrl(): ?string
    {
        return $this->apiUrl;
    }

    public function setApiUrl(string $apiUrl): self
    {
        $this->apiUrl = $apiUrl;

        return $this;
    }

    /**
     * @return Collection|PsInstall[]
     */
    public function getPsInstalls(): Collection
    {
        return $this->psInstalls;
    }

    public function addPsInstall(PsInstall $psInstall): self
    {
        if (!$this->psInstalls->contains($psInstall)) {
            $this->psInstalls[] = $psInstall;
        }

        return $this;
    }

    public function removePsInstall(PsInstall $psInstall): self
    {
        if ($this->psInstalls->contains($psInstall)) {
            $this->psInstalls->removeElement($psInstall);
        }

        return $this;
    }

    /**
     * @return Collection|PsModuleRequest[]
     */
    public function getPsModuleRequests(): Collection
    {
        return $this->psModuleRequests;
    }

    public function addPsModuleRequest(PsModuleRequest $psModuleRequest): self
    {
        if (!$this->psModuleRequests->contains($psModuleRequest)) {
            $this->psModuleRequests[] = $psModuleRequest;
            $psModuleRequest->setPsModule($this);
        }

        return $this;
    }

    public function removePsModuleRequest(PsModuleRequest $psModuleRequest): self
    {
        if ($this->psModuleRequests->contains($psModuleRequest)) {
            $this->psModuleRequests->removeElement($psModuleRequest);
            // set the owning side to null (unless already changed)
            if ($psModuleRequest->getPsModule() === $this) {
                $psModuleRequest->setPsModule(null);
            }
        }

        return $this;
    }
}
