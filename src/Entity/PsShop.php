<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PsShopRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="ps_shop_shortname", columns={"shortname"})})
 * @JMS\ExclusionPolicy("all")
 */
class PsShop
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotNull()
     */
    private $suburl;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     * @Assert\NotNull()
     */
    private $psShopId;

    /**
     * @ORM\Column(type="boolean")
     * @JMS\Expose()
     */
    private $active = false;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $shortname;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PsInstall", inversedBy="psShops")
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $psInstall;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSuburl(): ?string
    {
        return $this->suburl;
    }

    public function setSuburl(string $suburl): self
    {
        $this->suburl = $suburl;

        return $this;
    }

    public function getPsShopId(): ?int
    {
        return $this->psShopId;
    }

    public function setPsShopId(int $psShopId): self
    {
        $this->psShopId = $psShopId;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }

    public function getPsInstall(): ?PsInstall
    {
        return $this->psInstall;
    }

    public function setPsInstall(?PsInstall $psInstall): self
    {
        $this->psInstall = $psInstall;

        return $this;
    }
}
