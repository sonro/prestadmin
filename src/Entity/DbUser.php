<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DbUserRepository")
 * @ExclusionPolicy("all")
 */
class DbUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @Expose()
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var string|null
     * @SerializedName("password")
     * @Expose()
     * @Assert\NotBlank()
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PsDb", mappedBy="dbUser")
     */
    private $psDbs;

    /**
     * @ORM\Column(type="string", length=180)
     * @Expose()
     * @Assert\NotBlank()
     */
    private $dbHost;

    public function __construct()
    {
        $this->psDbs = new ArrayCollection();
        $this->dbHost = '127.0.0.1';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|PsDb[]
     */
    public function getPsDbs(): Collection
    {
        return $this->psDbs;
    }

    public function addPsDb(PsDb $psDb): self
    {
        if (!$this->psDbs->contains($psDb)) {
            $this->psDbs[] = $psDb;
            $psDb->setDbUser($this);
        }

        return $this;
    }

    public function removePsDb(PsDb $psDb): self
    {
        if ($this->psDbs->contains($psDb)) {
            $this->psDbs->removeElement($psDb);
            // set the owning side to null (unless already changed)
            if ($psDb->getDbUser() === $this) {
                $psDb->setDbUser(null);
            }
        }

        return $this;
    }

    public function getDbHost(): ?string
    {
        return $this->dbHost;
    }

    public function setDbHost(?string $dbHost): self
    {
        $defaultDbhost = '127.0.0.1';
        $dbHost = $dbHost ?: $defaultDbhost;
        $this->dbHost = $dbHost;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }
}
