<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PsDbRepository")
 * @JMS\ExclusionPolicy("all")
 */
class PsDb
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $dbName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DbUser", inversedBy="psDbs")
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $dbUser;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PsInstall", mappedBy="psDb")
     */
    private $psInstalls;

    public function __construct()
    {
        $this->psInstalls = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDbName(): ?string
    {
        return $this->dbName;
    }

    public function setDbName(string $dbName): self
    {
        $this->dbName = $dbName;

        return $this;
    }

    public function getDbUser(): ?DbUser
    {
        return $this->dbUser;
    }

    public function setDbUser(?DbUser $dbUser): self
    {
        $this->dbUser = $dbUser;

        return $this;
    }

    /**
     * @return Collection|PsInstall[]
     */
    public function getPsInstalls(): Collection
    {
        return $this->psInstalls;
    }

    public function addPsInstall(PsInstall $psInstall): self
    {
        if (!$this->psInstalls->contains($psInstall)) {
            $this->psInstalls[] = $psInstall;
            $psInstall->setPsDb($this);
        }

        return $this;
    }

    public function removePsInstall(PsInstall $psInstall): self
    {
        if ($this->psInstalls->contains($psInstall)) {
            $this->psInstalls->removeElement($psInstall);
            // set the owning side to null (unless already changed)
            if ($psInstall->getPsDb() === $this) {
                $psInstall->setPsDb(null);
            }
        }

        return $this;
    }
}
