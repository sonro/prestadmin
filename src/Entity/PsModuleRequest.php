<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PsModuleRequestRepository")
 * @JMS\ExclusionPolicy("all")
 */
class PsModuleRequest
{
    const HTTP_METHOD_GET = 1;
    const HTTP_METHOD_POST = 2;
    const HTTP_METHOD_PUT = 3;
    const HTTP_METHOD_PATCH = 4;
    const HTTP_METHOD_DELETE = 5;

    public static $HTTP_METHOD = [
        'GET' => self::HTTP_METHOD_GET,
        'POST' => self::HTTP_METHOD_POST,
        'PUT' => self::HTTP_METHOD_PUT,
        'PATCH' => self::HTTP_METHOD_PATCH,
        'DELETE' => self::HTTP_METHOD_DELETE,
    ];
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotNull()
     */
    private $endpointUrl;

    /**
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     * @JMS\Type("string")
     * @JMS\Accessor(getter="getMethodString", setter="setMethodString")
     * @Assert\NotBlank()
     */
    private $httpMethod;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @JMS\Expose()
     */
    private $parameters = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     * @JMS\Expose()
     */
    private $headers = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PsModule", inversedBy="psModuleRequests")
     * @Assert\NotBlank()
     */
    private $psModule;

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("psModule")
     */
    public function getPsModuleId()
    {
        return $this->psModule->getId();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEndpointUrl(): ?string
    {
        return $this->endpointUrl;
    }

    public function setEndpointUrl(string $endpointUrl): self
    {
        $this->endpointUrl = $endpointUrl;

        return $this;
    }

    public function getHttpMethod(): ?int
    {
        return $this->httpMethod;
    }

    public function setHttpMethod(int $httpMethod): self
    {
        $this->httpMethod = $httpMethod;

        return $this;
    }

    public function getMethodString(): ?string
    {
        return array_search($this->httpMethod, self::$HTTP_METHOD);
    }

    public function setMethodString(string $methodString): self
    {
        $this->httpMethod = self::$HTTP_METHOD[$methodString];

        return $this;
    }

    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    public function setParameters(?array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function addParameter(string $key, string $value): self
    {
        $this->parameters[$key] = $value;

        return $this;
    }

    public function getHeaders(): ?array
    {
        return $this->headers;
    }

    public function setHeaders(?array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    public function addHeader(string $key, string $value): self
    {
        $this->header[$key] = $value;

        return $this;
    }

    public function getPsModule(): ?PsModule
    {
        return $this->psModule;
    }

    public function setPsModule(?PsModule $psModule): self
    {
        $this->psModule = $psModule;

        return $this;
    }
}
