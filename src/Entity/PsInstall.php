<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PsInstallRepository")
 * @JMS\ExclusionPolicy("all")
 */
class PsInstall
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $baseUrl;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $adminFolder;

    /**
     * @ORM\Column(type="boolean")
     * @JMS\Expose()
     */
    private $multistore;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     */
    private $dbPrefix;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PsDb", inversedBy="psInstalls")
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $psDb;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PsShop", mappedBy="psInstall")
     * @JMS\Expose()
     */
    private $psShops;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PsModule", mappedBy="psInstalls")
     */
    private $psModules;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\OrderState", mappedBy="psInstalls")
     */
    private $orderStates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PsOrderState", mappedBy="psInstall")
     */
    private $psOrderStates;

    public function __construct()
    {
        $this->psModules = new ArrayCollection();
        $this->psShops = new ArrayCollection();
        $this->orderStates = new ArrayCollection();
        $this->psOrderStates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getBaseUrl(): ?string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $baseUrl): self
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    public function getAdminFolder(): ?string
    {
        return $this->adminFolder;
    }

    public function setAdminFolder(string $adminFolder): self
    {
        $this->adminFolder = $adminFolder;

        return $this;
    }

    public function getMultistore(): ?bool
    {
        return $this->multistore;
    }

    public function setMultistore(bool $multistore): self
    {
        $this->multistore = $multistore;

        return $this;
    }

    public function getDbPrefix(): ?string
    {
        return $this->dbPrefix;
    }

    public function setDbPrefix(?string $dbPrefix): self
    {
        if (empty($dbPrefix)) {
            $dbPrefix = 'ps_';
        }
        $this->dbPrefix = $dbPrefix;

        return $this;
    }

    public function getPsDb(): ?PsDb
    {
        return $this->psDb;
    }

    public function setPsDb(?PsDb $psDb): self
    {
        $this->psDb = $psDb;

        return $this;
    }

    /**
     * @return Collection|PsModule[]
     */
    public function getPsModules(): Collection
    {
        return $this->psModules;
    }

    public function addPsModule(PsModule $psModule): self
    {
        if (!$this->psModules->contains($psModule)) {
            $this->psModules[] = $psModule;
            $psModule->setPsInstall($this);
        }

        return $this;
    }

    public function removePsModule(PsModule $psModule): self
    {
        if ($this->psModules->contains($psModule)) {
            $this->psModules->removeElement($psModule);
            // set the owning side to null (unless already changed)
            if ($psModule->getPsInstall() === $this) {
                $psModule->setPsInstall(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PsShop[]
     */
    public function getPsShops(): Collection
    {
        return $this->psShops;
    }

    public function addPsShop(PsShop $psShop): self
    {
        if (!$this->psShops->contains($psShop)) {
            $this->psShops[] = $psShop;
            $psShop->setPsInstall($this);
        }

        return $this;
    }

    public function removePsShop(PsShop $psShop): self
    {
        if ($this->psShops->contains($psShop)) {
            $this->psShops->removeElement($psShop);
            // set the owning side to null (unless already changed)
            if ($psShop->getPsInstall() === $this) {
                $psShop->setPsInstall(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderState[]
     */
    public function getOrderStates(): Collection
    {
        return $this->orderStates;
    }

    public function addOrderState(OrderState $orderState): self
    {
        if (!$this->orderStates->contains($orderState)) {
            $this->orderStates[] = $orderState;
            $orderState->addOrderState($this);
        }

        return $this;
    }

    public function removeOrderState(OrderState $orderState): self
    {
        if ($this->orderStates->contains($orderState)) {
            $this->orderStates->removeElement($orderState);
            $orderState->removeOrderState($this);
        }

        return $this;
    }

    /**
     * @return Collection|PsOrderState[]
     */
    public function getPsOrderStates(): Collection
    {
        return $this->psOrderStates;
    }

    public function addPsOrderState(PsOrderState $psOrderState): self
    {
        if (!$this->psOrderStates->contains($psOrderState)) {
            $this->psOrderStates[] = $psOrderState;
            $psOrderState->setPsInstall($this);
        }

        return $this;
    }

    public function removePsOrderState(PsOrderState $psOrderState): self
    {
        if ($this->psOrderStates->contains($psOrderState)) {
            $this->psOrderStates->removeElement($psOrderState);
            // set the owning side to null (unless already changed)
            if ($psOrderState->getPsInstall() === $this) {
                $psOrderState->setPsInstall(null);
            }
        }

        return $this;
    }
}
