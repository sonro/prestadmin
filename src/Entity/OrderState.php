<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use App\Serialization\PsInstallIdsTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderStateRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="order_state_shortname", columns={"shortname"})})
 * @JMS\ExclusionPolicy("all")
 */
class OrderState
{
    use PsInstallIdsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=190)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180)
     * @JMS\Expose()
     * @Assert\NotBlank()
     */
    private $shortname;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PsInstall", inversedBy="orderStates")
     */
    private $psInstalls;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PsOrderState", mappedBy="orderState")
     */
    private $psOrderStates;

    public function __construct()
    {
        $this->psInstalls = new ArrayCollection();
        $this->psOrderStates = new ArrayCollection();
        $this->psInstalls = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * @return Collection|PsOrderState[]
     */
    public function getPsOrderStates(): Collection
    {
        return $this->psOrderStates;
    }

    public function addPsOrderState(PsOrderState $psOrderState): self
    {
        if (!$this->psOrderStates->contains($psOrderState)) {
            $this->psOrderStates[] = $psOrderState;
            $psOrderState->setOrderState($this);
        }

        return $this;
    }

    public function removePsOrderState(PsOrderState $psOrderState): self
    {
        if ($this->psOrderStates->contains($psOrderState)) {
            $this->psOrderStates->removeElement($psOrderState);
            // set the owning side to null (unless already changed)
            if ($psOrderState->getOrderState() === $this) {
                $psOrderState->setOrderState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PsInstall[]
     */
    public function getPsInstalls(): Collection
    {
        return $this->psInstalls;
    }

    public function addPsInstall(PsInstall $psInstall): self
    {
        if (!$this->psInstalls->contains($psInstall)) {
            $this->psInstalls[] = $psInstall;
        }

        return $this;
    }

    public function removePsInstall(PsInstall $psInstall): self
    {
        if ($this->psInstalls->contains($psInstall)) {
            $this->psInstalls->removeElement($psInstall);
        }

        return $this;
    }
}
