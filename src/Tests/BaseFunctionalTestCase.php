<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Employee;

abstract class BaseFunctionalTestCase extends WebTestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected static $entityManager;

    /**
     * @var string
     */
    private static $token;

    /**
     * @param string $uri
     * @param array  $parameters
     * @param array  $headers
     *
     * @return Response
     */
    protected function getRequest(string $uri, array $parameters = [], array $headers = [])
    {
        return $this->request('GET', $uri, $parameters, $headers);
    }

    /**
     * @param string $uri
     * @param string $body
     * @param array  $parameters
     * @param array  $headers
     *
     * @return Response
     */
    protected function postRequest(string $uri, string $body, array $parameters = [], array $headers = [])
    {
        $defaultHeaders = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
        $headers = array_merge($defaultHeaders, $headers);

        return $this->request('POST', $uri, $parameters, $headers, $body);
    }

    /**
     * @param string $uri
     * @param string $body
     * @param array  $parameters
     * @param array  $headers
     *
     * @return Response
     */
    protected function putRequest(string $uri, string $body, array $parameters = [], array $headers = [])
    {
        $defaultHeaders = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
        $headers = array_merge($defaultHeaders, $headers);

        return $this->request('PUT', $uri, $parameters, $headers, $body);
    }

    /**
     * @param string $uri
     * @param string $body
     * @param array  $parameters
     * @param array  $headers
     *
     * @return Response
     */
    protected function patchRequest(string $uri, string $body, array $parameters = [], array $headers = [])
    {
        $defaultHeaders = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
        $headers = array_merge($defaultHeaders, $headers);

        return $this->request('PATCH', $uri, $parameters, $headers, $body);
    }

    /**
     * @param string $uri
     * @param array  $parameters
     * @param array  $headers
     *
     * @return Response
     */
    protected function deleteRequest(string $uri, array $parameters = [], array $headers = [])
    {
        return $this->request('DELETE', $uri, $parameters, $headers);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $parameters
     * @param array  $headers
     * @param string $content
     *
     * @return Response
     */
    private function request(
        string $method,
        string $uri,
        array $parameters,
        array $headers,
        string $content = null
    ) {
        $server = [];
        foreach ($headers as $key => $value) {
            $server[strtoupper(str_replace('-', '_', $key))] = $value;
        }
        $server['HTTP_Authorization'] = sprintf('Bearer %s', self::$token);
        $client = static::createClient();
        $client->request($method, $uri, $parameters, [], $server, $content);

        return $client->getResponse();
    }

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        self::$entityManager = self::$container->get('doctrine')->getManager();
        self::$container->get('hautelook_alice.loader')->load(
            new Application(self::$kernel),
            self::$entityManager,
            [],
            self::$kernel->getEnvironment(),
            false,
            true,
            null
        );

        $user = self::$entityManager
            ->getRepository(Employee::class)
            ->findOneByEmail('test@email.com');

        self::$token = self::$container
            ->get('lexik_jwt_authentication.jwt_manager')
            ->create($user);
    }

    public static function tearDownAfterClass()
    {
        $purger = new ORMPurger(self::$entityManager);
        $purger->purge();
    }

    protected function tearDown()
    {
        // don't call parent class to keep kernel booted
    }
}
