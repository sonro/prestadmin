<?php

namespace App\Form;

use App\Entity\PsShop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PsShopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('suburl')
            ->add('psShopId')
            ->add('active')
            ->add('shortname')
            ->add('psInstall')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PsShop::class,
            'csrf_protection' => false,
        ]);
    }
}
