<?php

namespace App\Form;

use App\Entity\PsModuleRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\CallbackTransformer;

class PsModuleRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('endpointUrl', null, [
                'empty_data' => '',
            ])
            ->add('httpMethod', ChoiceType::class, [
                'choices' => [
                    'GET',
                    'POST',
                    'PUT',
                    'PATCH',
                    'DELETE',
                    1,
                    2,
                    3,
                    4,
                    5,
                ],
            ])
            ->add('psModule')
        ;

        $builder->get('httpMethod')
            ->addModelTransformer(new CallbackTransformer(
                function ($httpMethodAsInt) {
                    return array_search($httpMethodAsInt, PsModuleRequest::$HTTP_METHOD);
                },
                function ($httpMethodFromForm) {
                    if (is_int($httpMethodFromForm)) {
                        return $httpMethodFromForm;
                    }

                    return PsModuleRequest::$HTTP_METHOD[$httpMethodFromForm];
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PsModuleRequest::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}
