<?php

namespace App\Form;

use App\Entity\PsInstall;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PsInstallType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('version')
            ->add('baseUrl')
            ->add('adminFolder')
            ->add('multistore')
            ->add('dbPrefix', null, [
                'required' => 'false',
                'empty_data' => 'ps_',
            ])
            ->add('psDb')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PsInstall::class,
            'csrf_protection' => false,
        ]);
    }
}
