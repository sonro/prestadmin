<?php

namespace App\Form;

use App\Entity\DbUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DbUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('password', null, [
                'property_path' => 'plainPassword',
            ])
            ->add('dbHost', null, [
                'required' => 'false',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DbUser::class,
            'csrf_protection' => false,
        ]);
    }
}
