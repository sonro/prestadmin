<?php

namespace App\Form;

use App\Entity\PsOrderState;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PsOrderStateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('psId')
            ->add('psInstall')
            ->add('orderState')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PsOrderState::class,
            'csrf_protection' => false,
        ]);
    }
}
