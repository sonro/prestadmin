<?php

namespace App\Serialization;

use JMS\Serializer\Annotation as JMS;

trait PsInstallIdsTrait
{
    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("psInstalls")
     */
    public function getPsInstallIds()
    {
        $ids = [];
        foreach ($this->psInstalls as $install) {
            $ids[] = $install->getId();
        }

        return $ids;
    }
}
