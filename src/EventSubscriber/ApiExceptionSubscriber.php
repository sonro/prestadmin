<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use App\Api\ApiProblemException;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Api\ApiProblem;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PropertyAccess\Exception\InvalidArgumentException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    private $debug;

    private $baseUrl;

    public function __construct($debug)
    {
        $this->debug = $debug;
        $this->baseUrl = $this->setBaseUrl();
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (strpos($event->getRequest()->getPathInfo(), '/api') !== 0) {
            return;
        }
        $exception = $event->getException();
        if ($exception instanceof ApiProblemException) {
            $apiProblem = $exception->getApiProblem();
        } else {
            $apiProblem = $this->getApiProblemForException($exception);
            if ($apiProblem->getStatusCode() === 500 && $this->debug) {
                return;
            }
        }
        $data = $apiProblem->toArray();
        if ($data['type'] !== 'about:blank') {
            $data['type'] = $this->baseUrl.'/docs/error#'.$data['type'];
        }

        $response = new JsonResponse(
            $data,
            $apiProblem->getStatusCode(),
            ['Content-Type' => 'application/problem+json']
        );

        $event->setResponse($response);
    }

    private function getApiProblemForException($exception): ApiProblem
    {
        if ($exception instanceof HttpException) {
            $apiProblem = new ApiProblem(
                $exception->getStatusCode()
            );
            $apiProblem->set('detail', $exception->getMessage());
        } elseif ($exception instanceof InvalidArgumentException) {
            $apiProblem = new ApiProblem(
                400,
                ApiProblem::TYPE_VALIDATION_ERROR
            );
            $apiProblem->set('detail', $exception->getMessage());
        } elseif ($exception instanceof UniqueConstraintViolationException) {
            $apiProblem = new ApiProblem(
                400,
                ApiProblem::TYPE_DUPLICATE_ENTRY
            );
            $apiProblem->set('detail', $exception->getPrevious()->getMessage());
        } else {
            $apiProblem = new ApiProblem(
                500
            );
        }

        return $apiProblem;
    }

    private function setBaseUrl()
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http';
        } else {
            $protocol = 'http';
        }

        return $protocol.'://'.$_SERVER['HTTP_HOST'];
    }
}
