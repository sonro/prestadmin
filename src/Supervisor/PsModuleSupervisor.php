<?php

namespace App\Supervisor;

use App\Entity\PsModule;

class PsModuleSupervisor extends BaseSupervisor
{
    public function removePsModuleRequests(PsModule $psModule)
    {
        $psModuleRequests = $psModule->getPsModuleRequests();
        foreach ($psModuleRequests as $request) {
            $psModule->removePsModuleRequest($request);
        }
    }
}
