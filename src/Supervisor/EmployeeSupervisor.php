<?php

namespace App\Supervisor;

use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Employee;

class EmployeeSupervisor extends BaseSupervisor
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        parent::__construct($formFactory, $entityManager);
        $this->passwordEncoder = $passwordEncoder;
    }

    public function encodePassword(Employee $employee): Employee
    {
        $employee->setPassword(
            $this->passwordEncoder->encodePassword(
                $employee,
                $employee->getPlainPassword()
            )
        );

        return $employee;
    }
}
