<?php

namespace App\Supervisor;

use Symfony\Component\Form\FormFactoryInterface;
use App\Entity\DbUser;
use App\Utils\SonCrypt;
use Doctrine\ORM\EntityManagerInterface;

class DbUserSupervisor extends BaseSupervisor
{
    /**
     * @var SonCrypt
     */
    private $sonCrypt;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        SonCrypt $sonCrypt
    ) {
        parent::__construct($formFactory, $entityManager);
        $this->sonCrypt = $sonCrypt;
    }

    public function addFromArray(array $data): DbUser
    {
        $dbUser = new DbUser();
        $form = $this->formFactory->create(DbUserType::class, $dbUser);
        $this->processForm($form, $data);
        $this->encodePassword($dbUser);
        $this->persistEntity($dbUser);

        return $dbUser;
    }

    public function encodePassword(DbUser $dbUser)
    {
        $dbUser->setPassword($this->sonCrypt->encode($dbUser->getPlainPassword()));
    }

    public function decodePassword(DbUser $dbUser)
    {
        $dbUser->setPlainPassword($this->sonCrypt->decode($dbUser->getPassword()));
    }
}
