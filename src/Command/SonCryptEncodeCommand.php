<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use App\Utils\SonCrypt;

class SonCryptEncodeCommand extends Command
{
    protected static $defaultName = 'soncrypt:encode';

    /**
     * @var SonCrypt
     */
    private $sonCrypt;

    public function __construct(SonCrypt $sonCrypt)
    {
        $this->sonCrypt = $sonCrypt;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Encodes a string using SonCrypt')
            ->setHelp('Outputs a encoded SonCrypt string')
            ->addArgument('input', InputArgument::OPTIONAL, 'The string to encode')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $plainInput = $input->getArgument('input');
        if (!$plainInput) {
            $helper = $this->getHelper('question');
            $question = new Question('input: ');
            $question->setNormalizer(function ($value) {
                return $value ? trim($value) : '';
            });
            $question->setValidator(function ($answer) {
                if (strlen($answer) < 1) {
                    throw new \RuntimeException(
                        'Please enter a string to encode'
                    );
                }

                return $answer;
            });
            $question->setMaxAttempts(2);
            $plainInput = $helper->ask($input, $output, $question);
        }

        $output->writeln($this->sonCrypt->encode($plainInput));
    }
}
