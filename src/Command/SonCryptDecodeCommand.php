<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use App\Utils\SonCrypt;

class SonCryptDecodeCommand extends Command
{
    protected static $defaultName = 'soncrypt:decode';

    /**
     * @var SonCrypt
     */
    private $sonCrypt;

    public function __construct(SonCrypt $sonCrypt)
    {
        $this->sonCrypt = $sonCrypt;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Decodes a string using SonCrypt')
            ->setHelp('Outputs a decoded SonCrypt string')
            ->addArgument('input', InputArgument::OPTIONAL, 'The string to decode')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $encodedInput = $input->getArgument('input');
        if (!$encodedInput) {
            $helper = $this->getHelper('question');
            $question = new Question('input: ');
            $question->setNormalizer(function ($value) {
                return $value ? trim($value) : '';
            });
            $question->setValidator(function ($answer) {
                if (strlen($answer) < 1) {
                    throw new \RuntimeException(
                        'Please enter a string to decode'
                    );
                }

                return $answer;
            });
            $question->setMaxAttempts(2);
            $encodedInput = $helper->ask($input, $output, $question);
        }

        $output->writeln($this->sonCrypt->decode($encodedInput));
    }
}
