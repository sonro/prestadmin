<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\MakerBundle\Exception\RuntimeCommandException;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Component\Console\Question\Question;

class ApiMakeResourceCommand extends Command
{
    protected static $defaultName = 'api:make-resource';

    protected function configure()
    {
        $this
            ->setDescription('Creates or updates an API Resource')
            ->setHelp('Makes an Entity, Repository, Form, Supervisor and Controller for a Resource')
            ->addArgument('name', InputArgument::OPTIONAL, sprintf('Class name of the entity to create (e.g. <fg=yellow>%s</>)', Str::asClassName(Str::getRandomTerm())))
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $entityName = $input->getArgument('name');
        if (!$entityName) {
            $question = new Question(
                sprintf("Class name of the entity to create (e.g. <fg=yellow>%s</>)\n", Str::asClassName(Str::getRandomTerm()))
            );
            $question->setNormalizer(function ($value) {
                return $value ? trim($value) : null;
            });

            $helper = $this->getHelper('question');
            $entityName = $helper->ask($input, $output, $question);

            if ($entityName === null) {
                throw new RuntimeCommandException('Unable to create Resource');
            }
        }

        $this->runMakerCommand('make:entity', $entityName, ['name' => $entityName], $output);
        $this->runMakerCommand('make:api-resource', $entityName, ['bound-class' => $entityName], $output);
    }

    private function runMakerCommand(string $command, string $entityName, array $arguments, OutputInterface $output)
    {
        $command = $this->getApplication()->find($command);
        $arguments['command'] = $command;
        $makeInput = new ArrayInput($arguments);
        $makeReturnCode = $command->run($makeInput, $output);
        if ($makeReturnCode !== 0) {
            throw new RuntimeCommandException("Maker bundle command failed with code: $makeReturnCode");
        }
    }
}
