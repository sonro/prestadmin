<?php

namespace App\Utils;

use App\Entity\Configuration;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Doctrine\ORM\EntityManagerInterface;

class ConfigurationInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $key;

    public function __construct(
        EntityManagerInterface $entityManager,
        AdapterInterface $cache,
        $cacheKey
    ) {
        $this->entityManager = $entityManager;
        $this->key = $cacheKey;
        $this->cache = $cache;
        $this->data = $this->getCachedData();
    }

    /**
     * Set value.
     *
     * @param string      $name
     * @param string|null $value
     */
    public function set(string $name, ?string $value)
    {
        $this->storeConfiguration($name, $value);
        $this->updateCache();
    }

    /**
     * Set multiple values.
     *
     * @param array $data
     */
    public function setMultiple(array $data)
    {
        foreach ($data as $name => $value) {
            $this->storeConfiguration($name, $value);
        }

        $this->updateCache();
    }

    /**
     * Delete one configuration entry.
     *
     * @param string $name
     */
    public function delete(string $name)
    {
        $configuration = $this->entityManager
            ->getRepository(Configuration::class)
            ->findOneByName($name);
        if ($configuration === null) {
            return;
        }

        $this->entityManager->remove($configuration);
        $this->entityManager->flush();
        unset($this->data[$name]);

        $this->updateCache();
    }

    /**
     * Get data array.
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    private function updateCache()
    {
        $item = $this->getCachedItem();
        $item->set($this->data);
        $this->cache->save($item);
    }

    private function storeConfiguration(string $name, ?string $value)
    {
        $configuration = $this->entityManager
            ->getRepository(Configuration::class)
            ->findOneByName($name);
        if ($configuration === null) {
            $configuration = new Configuration();
            $configuration->setName($name);
        }

        $configuration->setValue($value);

        $this->entityManager->persist($configuration);
        $this->entityManager->flush();

        $this->data[$name] = $value;
    }

    /**
     * Get value.
     *
     * @param string $name
     *
     * @return string|null
     */
    public function get(string $name): ?string
    {
        if (!isset($this->data[$name])) {
            return null;
        }

        return $this->data[$name];
    }

    private function getCachedData()
    {
        $item = $this->getCachedItem($this->key);
        if (!$item->isHit()) {
            $configurations = $this->entityManager
                ->getRepository(Configuration::class)
                ->findAll();

            $data = [];
            foreach ($configurations as $configuration) {
                $data[$configuration->getName()] = $configuration->getValue();
            }

            $item->set($data);
            $this->cache->save($item);
        }

        return $item->get();
    }

    private function getCachedItem()
    {
        return $this->cache->getItem($this->key);
    }
}
