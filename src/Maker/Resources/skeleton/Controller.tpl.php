<?= "<?php\n"; ?>

namespace <?= $namespace; ?>;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
<?php foreach ($boundUseStatements as $className): ?>
use <?= $className; ?>;
<?php endforeach; ?>

class <?= $class_name; ?> extends ApiResourceController
{
    /**
     * @var <?= $boundClassNames['supervisor']; ?> 
     */
    protected $supervisor;

    public function __construct(
        SerializerInterface $serializer,
        <?= $boundClassNames['supervisor']; ?> $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = <?= $boundClassNames['entity']; ?>::class;
        $this->entityTypeClass = <?= $boundClassNames['form']; ?>::class;
        $this->resourceName = '<?= lcfirst($boundClassNames['entity']); ?>';
    }

    /**
     * @Route("<?= $baseRoute; ?>", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("<?= $baseRoute; ?>", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request);
    }

    /**
     * @Route("<?= $baseRoute; ?>/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("<?= $baseRoute; ?>/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("<?= $baseRoute; ?>/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse($request, $id);
    }
}
