<?php

namespace App\Maker;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Common\Annotations\Annotation;
use JMS\SerializerBundle\JMSSerializerBundle;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Doctrine\DoctrineHelper;
use Symfony\Bundle\MakerBundle\Exception\RuntimeCommandException;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Bundle\MakerBundle\Util\ClassDetails;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Validation;

class MakeApiResource extends AbstractMaker
{
    /**
     * @var DoctrineHelper
     */
    private $entityHelper;

    public function __construct(DoctrineHelper $entityHelper)
    {
        $this->entityHelper = $entityHelper;
    }

    public static function getCommandName(): string
    {
        return 'make:api-resource';
    }

    public function configureCommand(Command $command, InputConfiguration $inputConfig)
    {
        $command
            ->setDescription('Creates or edits new Api Resource classes')
            ->addArgument(
                'bound-class',
                InputArgument::OPTIONAL,
                sprintf(
                    'The name of Entity or fully qualified model class name that the new form will be bound to'
                )
            )
            ->setHelp(file_get_contents(__DIR__.'/Resources/help/MakeApiResource.txt'))
        ;

        $inputConfig->setArgumentAsNonInteractive('bound-class');
    }

    public function interact(InputInterface $input, ConsoleStyle $io, Command $command)
    {
        if (null === $input->getArgument('bound-class')) {
            $argument = $command->getDefinition()->getArgument('bound-class');
            $entities = $this->entityHelper->getEntitiesForAutocomplete();
            $question = new Question($argument->getDescription());
            $question->setValidator(function ($answer) use ($entities) {return Validator::existsOrNull($answer, $entities); });
            $question->setAutocompleterValues($entities);
            $question->setMaxAttempts(3);
            $input->setArgument('bound-class', $io->askQuestion($question));
        }
    }

    public function configureDependencies(DependencyBuilder $dependencies)
    {
        $dependencies->addClassDependency(
            AbstractType::class,
            'form'
        );
        $dependencies->addClassDependency(
            Validation::class,
            'validator',
            false
        );
        $dependencies->addClassDependency(
            DoctrineBundle::class,
            'orm',
            false
        );
        $dependencies->addClassDependency(
            Annotation::class,
            'annotations'
        );
        $dependencies->addClassDependency(
            JMSSerializerBundle::class,
            'jms'
        );
    }

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        $boundClass = $input->getArgument('bound-class');
        if (null === $boundClass) {
            throw new RuntimeCommandException(sprintf('Please specify an Entity'));
        }

        // create the ClassName objects
        $entityClassNameDetails = $generator->createClassNameDetails(
            $boundClass,
            'Entity\\'
        );
        $classExists = class_exists($entityClassNameDetails->getFullName());
        if (!$classExists) {
            throw new RuntimeCommandException(
                sprintf(
                    'The Entity <info>%s</info> does not exist. Please create it first',
                    $entityClassNameDetails->getFullName()
                )
            );
        }
        $formClassNameDetails = $generator->createClassNameDetails(
            $boundClass,
            'Form\\',
            'Type'
        );
        $supervisorClassNameDetails = $generator->createClassNameDetails(
            $boundClass,
            'Supervisor\\',
            'Supervisor'
        );
        $controllerClassNameDetails = $generator->createClassNameDetails(
            $boundClass,
            'Controller\\Api\\Resource',
            'Controller'
        );

        // generate form fields
        $doctrineEntityDetails = $this->entityHelper->createDoctrineDetails($entityClassNameDetails->getFullName());
        if (null !== $doctrineEntityDetails) {
            $formFields = $doctrineEntityDetails->getFormFields();
        } else {
            $classDetails = new ClassDetails($entityClassNameDetails->getFullName());
            $formFields = $classDetails->getFormFields();
        }

        // genearte form class
        $this->renderForm(
            $generator,
            $formClassNameDetails,
            $formFields,
            $entityClassNameDetails
        );

        // generate supervisor class
        $generator->generateClass(
            $supervisorClassNameDetails->getFullName(),
            __DIR__.'/Resources/skeleton/Supervisor.tpl.php'
        );

        // bind Entity's class, form class and supervisor classes:
        //      for 'use' statement usage
        $boundUseStatements = [
            $entityClassNameDetails->getFullName(),
            $formClassNameDetails->getFullName(),
            $supervisorClassNameDetails->getFullName(),
        ];
        //      for method and route parameters
        $boundClassNames = [
            'supervisor' => $supervisorClassNameDetails->getShortName(),
            'form' => $formClassNameDetails->getShortName(),
            'entity' => $entityClassNameDetails->getShortName(),
        ];

        // generate controller class
        $baseRoute = '/api/resource/'.strtolower($entityClassNameDetails->getShortName());
        $generator->generateController(
            $controllerClassNameDetails->getFullName(),
            __DIR__.'/Resources/skeleton/Controller.tpl.php',
            [
                'boundClassNames' => $boundClassNames,
                'boundUseStatements' => $boundUseStatements,
                'baseRoute' => $baseRoute,
            ]
        );

        // finalize changes and print to console screen
        $generator->writeChanges();
        $this->writeSuccessMessage($io);
    }

    /**
     * Reder Form.
     *
     * Taken from Symfony\Bundle\MakerBundle\Renderer
     * Added custom generateClass call to use new class template
     *
     * @param Generator        $generator
     * @param ClassNameDetails $formClassDetails
     * @param array            $formFields
     * @param ClassNameDetails $boundClassDetails
     * @param array            $constraintClasses
     */
    private function renderForm(
        Generator $generator,
        ClassNameDetails $formClassDetails,
        array $formFields,
        ClassNameDetails $boundClassDetails = null,
        array $constraintClasses = []
    ) {
        $fieldTypeUseStatements = [];
        $fields = [];
        foreach ($formFields as $name => $fieldTypeOptions) {
            $fieldTypeOptions = $fieldTypeOptions ?? ['type' => null, 'options_code' => null];

            if (isset($fieldTypeOptions['type'])) {
                $fieldTypeUseStatements[] = $fieldTypeOptions['type'];
                $fieldTypeOptions['type'] = Str::getShortClassName($fieldTypeOptions['type']);
            }

            $fields[$name] = $fieldTypeOptions;
        }

        $generator->generateClass(
            $formClassDetails->getFullName(),
            __DIR__.'/Resources/skeleton/Type.tpl.php',
            [
                'bounded_full_class_name' => $boundClassDetails ? $boundClassDetails->getFullName() : null,
                'bounded_class_name' => $boundClassDetails ? $boundClassDetails->getShortName() : null,
                'form_fields' => $fields,
                'field_type_use_statements' => $fieldTypeUseStatements,
                'constraint_use_statements' => $constraintClasses,
            ]
        );
    }
}
