<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190117115630 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ps_order_state (id INT AUTO_INCREMENT NOT NULL, ps_install_id INT DEFAULT NULL, order_state_id INT DEFAULT NULL, ps_id INT NOT NULL, INDEX IDX_398F085FF923450D (ps_install_id), INDEX IDX_398F085FE420DE70 (order_state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_state (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(190) NOT NULL, shortname VARCHAR(180) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_state_ps_install (order_state_id INT NOT NULL, ps_install_id INT NOT NULL, INDEX IDX_C5CC543FE420DE70 (order_state_id), INDEX IDX_C5CC543FF923450D (ps_install_id), PRIMARY KEY(order_state_id, ps_install_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ps_order_state ADD CONSTRAINT FK_398F085FF923450D FOREIGN KEY (ps_install_id) REFERENCES ps_install (id)');
        $this->addSql('ALTER TABLE ps_order_state ADD CONSTRAINT FK_398F085FE420DE70 FOREIGN KEY (order_state_id) REFERENCES order_state (id)');
        $this->addSql('ALTER TABLE order_state_ps_install ADD CONSTRAINT FK_C5CC543FE420DE70 FOREIGN KEY (order_state_id) REFERENCES order_state (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_state_ps_install ADD CONSTRAINT FK_C5CC543FF923450D FOREIGN KEY (ps_install_id) REFERENCES ps_install (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ps_order_state DROP FOREIGN KEY FK_398F085FE420DE70');
        $this->addSql('ALTER TABLE order_state_ps_install DROP FOREIGN KEY FK_C5CC543FE420DE70');
        $this->addSql('DROP TABLE ps_order_state');
        $this->addSql('DROP TABLE order_state');
        $this->addSql('DROP TABLE order_state_ps_install');
    }
}
