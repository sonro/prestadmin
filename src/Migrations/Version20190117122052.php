<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190117122052 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ps_module (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, api_url VARCHAR(180) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ps_module_ps_install (ps_module_id INT NOT NULL, ps_install_id INT NOT NULL, INDEX IDX_C7992489398B0F93 (ps_module_id), INDEX IDX_C7992489F923450D (ps_install_id), PRIMARY KEY(ps_module_id, ps_install_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ps_module_request (id INT AUTO_INCREMENT NOT NULL, ps_module_id INT DEFAULT NULL, name VARCHAR(180) NOT NULL, endpoint_url VARCHAR(180) NOT NULL, http_method INT NOT NULL, parameters LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', headers LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', INDEX IDX_C0B3D7CB398B0F93 (ps_module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ps_module_ps_install ADD CONSTRAINT FK_C7992489398B0F93 FOREIGN KEY (ps_module_id) REFERENCES ps_module (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ps_module_ps_install ADD CONSTRAINT FK_C7992489F923450D FOREIGN KEY (ps_install_id) REFERENCES ps_install (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ps_module_request ADD CONSTRAINT FK_C0B3D7CB398B0F93 FOREIGN KEY (ps_module_id) REFERENCES ps_module (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ps_module_ps_install DROP FOREIGN KEY FK_C7992489398B0F93');
        $this->addSql('ALTER TABLE ps_module_request DROP FOREIGN KEY FK_C0B3D7CB398B0F93');
        $this->addSql('DROP TABLE ps_module');
        $this->addSql('DROP TABLE ps_module_ps_install');
        $this->addSql('DROP TABLE ps_module_request');
    }
}
