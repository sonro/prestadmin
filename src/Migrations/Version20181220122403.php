<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181220122403 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ps_install (id INT AUTO_INCREMENT NOT NULL, ps_db_id INT DEFAULT NULL, name VARCHAR(180) NOT NULL, base_url VARCHAR(180) NOT NULL, admin_folder VARCHAR(180) NOT NULL, multistore TINYINT(1) NOT NULL, db_prefix VARCHAR(180) NOT NULL, INDEX IDX_E24D91938DB7CC4A (ps_db_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE db_host (id INT AUTO_INCREMENT NOT NULL, address VARCHAR(180) NOT NULL, name VARCHAR(180) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ps_shop (id INT AUTO_INCREMENT NOT NULL, ps_install_id INT DEFAULT NULL, name VARCHAR(180) NOT NULL, suburl VARCHAR(180) NOT NULL, ps_shop_id INT NOT NULL, active TINYINT(1) NOT NULL, INDEX IDX_CBDFBB9EF923450D (ps_install_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE db_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, password VARCHAR(180) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ps_db (id INT AUTO_INCREMENT NOT NULL, db_user_id INT DEFAULT NULL, db_host_id INT DEFAULT NULL, name VARCHAR(180) NOT NULL, db_name VARCHAR(180) NOT NULL, INDEX IDX_83B7DFFAFF1788DF (db_user_id), INDEX IDX_83B7DFFA47C18ACF (db_host_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ps_install ADD CONSTRAINT FK_E24D91938DB7CC4A FOREIGN KEY (ps_db_id) REFERENCES ps_db (id)');
        $this->addSql('ALTER TABLE ps_shop ADD CONSTRAINT FK_CBDFBB9EF923450D FOREIGN KEY (ps_install_id) REFERENCES ps_install (id)');
        $this->addSql('ALTER TABLE ps_db ADD CONSTRAINT FK_83B7DFFAFF1788DF FOREIGN KEY (db_user_id) REFERENCES db_user (id)');
        $this->addSql('ALTER TABLE ps_db ADD CONSTRAINT FK_83B7DFFA47C18ACF FOREIGN KEY (db_host_id) REFERENCES db_host (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ps_shop DROP FOREIGN KEY FK_CBDFBB9EF923450D');
        $this->addSql('ALTER TABLE ps_db DROP FOREIGN KEY FK_83B7DFFA47C18ACF');
        $this->addSql('ALTER TABLE ps_db DROP FOREIGN KEY FK_83B7DFFAFF1788DF');
        $this->addSql('ALTER TABLE ps_install DROP FOREIGN KEY FK_E24D91938DB7CC4A');
        $this->addSql('DROP TABLE ps_install');
        $this->addSql('DROP TABLE db_host');
        $this->addSql('DROP TABLE ps_shop');
        $this->addSql('DROP TABLE db_user');
        $this->addSql('DROP TABLE ps_db');
    }
}
