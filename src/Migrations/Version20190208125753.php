<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190208125753 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX uniq_cbdfbb9e64082763 ON ps_shop');
        $this->addSql('CREATE UNIQUE INDEX ps_shop_shortname ON ps_shop (shortname)');
        $this->addSql('DROP INDEX uniq_200da60664082763 ON order_state');
        $this->addSql('CREATE UNIQUE INDEX order_state_shortname ON order_state (shortname)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX order_state_shortname ON order_state');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_200DA60664082763 ON order_state (shortname)');
        $this->addSql('DROP INDEX ps_shop_shortname ON ps_shop');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CBDFBB9E64082763 ON ps_shop (shortname)');
    }
}
