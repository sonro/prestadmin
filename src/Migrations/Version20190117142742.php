<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190117142742 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ps_install_ps_module (ps_install_id INT NOT NULL, ps_module_id INT NOT NULL, INDEX IDX_C047FB55F923450D (ps_install_id), INDEX IDX_C047FB55398B0F93 (ps_module_id), PRIMARY KEY(ps_install_id, ps_module_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ps_install_ps_module ADD CONSTRAINT FK_C047FB55F923450D FOREIGN KEY (ps_install_id) REFERENCES ps_install (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ps_install_ps_module ADD CONSTRAINT FK_C047FB55398B0F93 FOREIGN KEY (ps_module_id) REFERENCES ps_module (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE ps_module_ps_install');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ps_module_ps_install (ps_module_id INT NOT NULL, ps_install_id INT NOT NULL, INDEX IDX_C7992489F923450D (ps_install_id), INDEX IDX_C7992489398B0F93 (ps_module_id), PRIMARY KEY(ps_module_id, ps_install_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE ps_module_ps_install ADD CONSTRAINT FK_C7992489398B0F93 FOREIGN KEY (ps_module_id) REFERENCES ps_module (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ps_module_ps_install ADD CONSTRAINT FK_C7992489F923450D FOREIGN KEY (ps_install_id) REFERENCES ps_install (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE ps_install_ps_module');
    }
}
