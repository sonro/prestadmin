<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181230122646 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE db_user DROP FOREIGN KEY FK_B13D489647C18ACF');
        $this->addSql('ALTER TABLE ps_db DROP FOREIGN KEY FK_83B7DFFA47C18ACF');
        $this->addSql('DROP TABLE db_host');
        $this->addSql('DROP INDEX IDX_B13D489647C18ACF ON db_user');
        $this->addSql('ALTER TABLE db_user ADD db_host VARCHAR(180) NOT NULL, DROP db_host_id');
        $this->addSql('DROP INDEX IDX_83B7DFFA47C18ACF ON ps_db');
        $this->addSql('ALTER TABLE ps_db DROP db_host_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE db_host (id INT AUTO_INCREMENT NOT NULL, address VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX UNIQ_F3898D22D4E6F81 (address), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE db_user ADD db_host_id INT DEFAULT NULL, DROP db_host');
        $this->addSql('ALTER TABLE db_user ADD CONSTRAINT FK_B13D489647C18ACF FOREIGN KEY (db_host_id) REFERENCES db_host (id)');
        $this->addSql('CREATE INDEX IDX_B13D489647C18ACF ON db_user (db_host_id)');
        $this->addSql('ALTER TABLE ps_db ADD db_host_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ps_db ADD CONSTRAINT FK_83B7DFFA47C18ACF FOREIGN KEY (db_host_id) REFERENCES db_host (id)');
        $this->addSql('CREATE INDEX IDX_83B7DFFA47C18ACF ON ps_db (db_host_id)');
    }
}
