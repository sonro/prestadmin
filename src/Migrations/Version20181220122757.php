<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181220122757 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE db_user ADD db_host_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE db_user ADD CONSTRAINT FK_B13D489647C18ACF FOREIGN KEY (db_host_id) REFERENCES db_host (id)');
        $this->addSql('CREATE INDEX IDX_B13D489647C18ACF ON db_user (db_host_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE db_user DROP FOREIGN KEY FK_B13D489647C18ACF');
        $this->addSql('DROP INDEX IDX_B13D489647C18ACF ON db_user');
        $this->addSql('ALTER TABLE db_user DROP db_host_id');
    }
}
