<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Api\ApiProblem;
use App\Api\ApiProblemException;

abstract class ApiController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    protected function apiResponse($data, $httpCode = 200, $headers = array())
    {
        $headers['Content-Type'] = 'application/json';
        if ($data) {
            $json = $this->serializer->serialize($data, 'json');
        } else {
            $json = null;
        }
        $response = new Response($json, $httpCode, $headers);

        return $response;
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }

    protected function throwApiProblemValidationException(FormInterface $form = null, $errors = [])
    {
        $apiProblem = new ApiProblem(
            400,
            ApiProblem::TYPE_VALIDATION_ERROR
        );
        if ($form !== null) {
            $errors[] = $this->getErrorsFromForm($form);
        }
        if (!empty($errors)) {
            $apiProblem->set('errors', $errors);
        }
        throw new ApiProblemException($apiProblem);
    }

    protected function processForm(Request $request, FormInterface $form, $data = null)
    {
        $data = $data ?: $this->decodeJson($request->getContent());
        $clearMissing = $request->getMethod() !== 'PATCH';
        $form->submit($data, $clearMissing);

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        return $data;
    }

    protected function decodeJson(string $content, ?string $deserializeType = null)
    {
        if ($deserializeType === null) {
            $data = json_decode($content, true);
        } else {
            $data = $this->serializer->deserialize($content, $deserializeType, 'json');
        }
        if ($data === null) {
            $apiProblem = new ApiProblem(
                400,
                ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT
            );
            throw new ApiProblemException($apiProblem);
        }

        return $data;
    }
}
