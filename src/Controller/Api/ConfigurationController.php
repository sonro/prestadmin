<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Entity\Configuration;
use App\Utils\ConfigurationInterface;

class ConfigurationController extends ApiController
{
    /**
     * @var ConfigurationInterface
     */
    private $configuration;

    public function __construct(
        SerializerInterface $serializer,
        ConfigurationInterface $configuration
    ) {
        parent::__construct($serializer);
        $this->configuration = $configuration;
    }

    /**
     * @Route("/api/configuration", methods={"GET"})
     */
    public function list()
    {
        $data = $this->configuration->getData();

        return $this->apiResponse($data);
    }

    /**
     * @Route("/api/configuration/{key}", methods={"GET"})
     */
    public function show($key)
    {
        $data = $this->configuration->get($key);
        if ($data === null) {
            return $this->apiResponse(null, 204);
        }

        return $this->apiResponse($data);
    }

    /**
     * @Route("/api/configuration", methods={"POST"})
     */
    public function post(Request $request)
    {
        $data = $this->decodeJson($request->getContent());
        $hasError = false;
        if (!is_array($data)) {
            $hasError = true;
        } else {
            foreach ($data as $value) {
                if (is_array($value)) {
                    $hasError = true;
                    break;
                }
            }
        }
        if ($hasError) {
            $errors[] = "Must be a list of 'key': 'value' pairs";
            $this->throwApiProblemValidationException(null, $errors);
        }

        $this->configuration->setMultiple($data);

        return $this->apiResponse(null, 204);
    }

    /**
     * @Route("/api/configuration/{key}", methods={"PUT"})
     */
    public function put(Request $request, $key)
    {
        $data = $this->decodeJson($request->getContent());
        if (!is_string($data)) {
            $errors[] = 'Must be a single string value';
            $this->throwApiProblemValidationException(null, $errors);
        }
        $this->configuration->set($key, $data);

        return $this->apiResponse(null, 204);
    }

    /**
     * @Route("/api/configuration/{key}/unset", methods={"PUT", "PATCH"})
     */
    public function unset($key)
    {
        $this->configuration->set($key, null);

        return $this->apiResponse(null, 204);
    }

    /**
     * @Route("/api/configuration/{key}", methods={"DELETE"})
     */
    public function delete($key)
    {
        $this->configuration->delete($key);

        return $this->apiResponse(null, 204);
    }
}
