<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Entity\PsModuleRequest;
use App\Form\PsModuleRequestType;
use App\Supervisor\PsModuleRequestSupervisor;

class PsModuleRequestController extends ApiResourceController
{
    /**
     * @var PsModuleRequestSupervisor
     */
    protected $supervisor;

    public function __construct(
        SerializerInterface $serializer,
        PsModuleRequestSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = PsModuleRequest::class;
        $this->entityTypeClass = PsModuleRequestType::class;
        $this->resourceName = 'psModuleRequest';
    }

    /**
     * @Route("/api/resource/psmodulerequest", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("/api/resource/psmodulerequest", methods={"POST"})
     */
    public function add(Request $request)
    {
        $this->data = $this->decodeJson($request->getContent());
        $data = $this->data;
        $cb = function (PsModuleRequest $psModuleReqeust) use ($data) {
            if (array_key_exists('parameters', $data)) {
                $psModuleReqeust->setParameters($data['parameters']);
            }
            if (array_key_exists('headers', $data)) {
                $psModuleReqeust->setHeaders($data['headers']);
            }
        };

        return $this->addResponse($request, $cb);
    }

    /**
     * @Route("/api/resource/psmodulerequest/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("/api/resource/psmodulerequest/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("/api/resource/psmodulerequest/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        $this->data = $this->decodeJson($request->getContent());
        $data = $this->data;
        $cb = function (PsModuleRequest $psModuleReqeust) use ($data) {
            if (array_key_exists('parameters', $data)) {
                $psModuleReqeust->setParameters($data['parameters']);
            }
            if (array_key_exists('headers', $data)) {
                $psModuleReqeust->setHeaders($data['headers']);
            }
        };

        return $this->updateResponse($request, $id, null, $cb);
    }
}
