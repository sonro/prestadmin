<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\DbUser;
use Symfony\Component\HttpFoundation\Request;
use App\Form\DbUserType;
use App\Supervisor\DbUserSupervisor;

class DbUserController extends ApiResourceController
{
    /**
     * @var DbUserSupervisor
     */
    protected $supervisor;

    private $decodeCallback;
    private $encodeCallback;

    public function __construct(
        \JMS\Serializer\SerializerInterface $serializer,
        DbUserSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = DbUser::class;
        $this->entityTypeClass = DbUserType::class;
        $this->resourceName = 'dbUser';

        $this->decodeCallback = function ($dbUser) use ($supervisor) {
            $supervisor->decodePassword($dbUser);
        };
        $this->encodeCallback = function ($dbUser) use ($supervisor) {
            $supervisor->encodePassword($dbUser);
        };
    }

    /**
     * @Route("/api/resource/dbuser", methods={"GET"})
     */
    public function list(Request $request)
    {
        $showPassword = $request->query->has('show-password');
        if ($showPassword) {
            $supervisor = $this->supervisor;
            $resourceFunction = function ($dbUsers) use ($supervisor) {
                foreach ($dbUsers as $dbUser) {
                    $supervisor->decodePassword($dbUser);
                }
            };
        } else {
            $resourceFunction = null;
        }

        return $this->listResponse($request, $resourceFunction);
    }

    /**
     * @Route("/api/resource/dbuser", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request, $this->encodeCallback);
    }

    /**
     * @Route("/api/resource/dbuser/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id, $this->decodeCallback);
    }

    /**
     * @Route("/api/resource/dbuser/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("/api/resource/dbuser/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse(
            $request,
            $id,
            $this->decodeCallback,
            $this->encodeCallback
        );
    }
}
