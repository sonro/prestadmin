<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Supervisor\PsInstallSupervisor;
use App\Entity\PsInstall;
use App\Form\PsInstallType;

class PsInstallController extends ApiResourceController
{
    /**
     * @var PsInstallSupervisor
     */
    protected $supervisor;

    public function __construct(
        \JMS\Serializer\SerializerInterface $serializer,
        PsInstallSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = PsInstall::class;
        $this->entityTypeClass = PsInstallType::class;
        $this->resourceName = 'psInstall';
    }

    /**
     * @Route("/api/resource/psinstall", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("/api/resource/psinstall", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request);
    }

    /**
     * @Route("/api/resource/psinstall/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("/api/resource/psinstall/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("/api/resource/psinstall/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse($request, $id);
    }
}
