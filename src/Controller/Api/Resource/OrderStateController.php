<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Entity\OrderState;
use App\Form\OrderStateType;
use App\Supervisor\OrderStateSupervisor;

class OrderStateController extends ApiResourceController
{
    /**
     * @var OrderStateSupervisor
     */
    protected $supervisor;

    public function __construct(
        SerializerInterface $serializer,
        OrderStateSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = OrderState::class;
        $this->entityTypeClass = OrderStateType::class;
        $this->resourceName = 'orderState';
    }

    /**
     * @Route("/api/resource/orderstate", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("/api/resource/orderstate", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request);
    }

    /**
     * @Route("/api/resource/orderstate/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("/api/resource/orderstate/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("/api/resource/orderstate/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse($request, $id);
    }
}
