<?php

namespace App\Controller\Api\Resource;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\Api\ApiController;

abstract class ApiResourceController extends ApiController
{
    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var string
     */
    protected $entityTypeClass;

    /**
     * @var string
     */
    protected $resourceName;

    /**
     * Set type on each Controller.
     */
    protected $supervisor;

    /**
     * JSON-Decoded data from request.
     *
     * @var array
     */
    protected $data;

    public function __construct(SerializerInterface $serializer)
    {
        $this->data = null;
        parent::__construct($serializer);
    }

    protected function listResponse(Request $request, $resourceFunction = null)
    {
        $resources = $this->getEntityCollection();

        $this->runResourceFunction($resourceFunction, $resources);

        return $this->apiResponse([
            $this->resourceName.'s' => $resources,
        ]);
    }

    protected function addResponse(Request $request, $resourceFunction = null)
    {
        $resource = new $this->entityClass();
        $form = $this->createForm($this->entityTypeClass, $resource);
        $this->processForm($request, $form, $this->data);

        $this->runResourceFunction($resourceFunction, $resource);

        $this->supervisor->persistEntity($resource);

        $location = $this->generateUrl(
            'app_api_resource_'.strtolower($this->resourceName).'_show',
            ['id' => $resource->getId()]
        );

        return $this->apiResponse($resource, 201, [
            'Location' => $location,
        ]);
    }

    protected function showResponse($id, $resourceFunction = null)
    {
        $resource = $this->getEntityFromId($id);

        $this->runResourceFunction($resourceFunction, $resource);

        return $this->apiResponse($resource);
    }

    protected function deleteResponse($id, $resourceFunction = null)
    {
        $resource = $this->getEntityFromId($id);

        $this->runResourceFunction($resourceFunction, $resource);

        $this->supervisor->removeEntity($resource);

        return $this->apiResponse(null, 204);
    }

    protected function updateResponse(
        Request $request,
        $id,
        $preResourceFunction = null,
        $postResourceFunction = null
    ) {
        $resource = $this->getEntityFromId($id);

        $this->runResourceFunction($preResourceFunction, $resource);

        $form = $this->createForm($this->entityTypeClass, $resource);
        $this->processForm($request, $form, $this->data);

        $this->runResourceFunction($postResourceFunction, $resource);

        $this->supervisor->persistEntity($resource);

        return $this->apiResponse($resource);
    }

    private function runResourceFunction($resourceFunction, $resource)
    {
        if ($resourceFunction) {
            call_user_func($resourceFunction, $resource);
        }
    }

    protected function getEntityFromId($id)
    {
        $entity = $this->getDoctrine()
            ->getRepository($this->entityClass)
            ->find($id);
        if ($entity === null) {
            throw $this->createNotFoundException(
                'No '.$this->entityClass." with the id: $id"
            );
        }

        return $entity;
    }

    protected function getEntityCollection()
    {
        return $this->getDoctrine()
            ->getRepository($this->entityClass)
            ->findAll();
    }
}
