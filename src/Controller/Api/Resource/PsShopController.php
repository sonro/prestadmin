<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Entity\PsShop;
use App\Form\PsShopType;
use App\Supervisor\PsShopSupervisor;

class PsShopController extends ApiResourceController
{
    /**
     * @var PsShopSupervisor
     */
    protected $supervisor;

    public function __construct(
        SerializerInterface $serializer,
        PsShopSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = PsShop::class;
        $this->entityTypeClass = PsShopType::class;
        $this->resourceName = 'psShop';
    }

    /**
     * @Route("/api/resource/psshop", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("/api/resource/psshop", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request);
    }

    /**
     * @Route("/api/resource/psshop/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("/api/resource/psshop/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("/api/resource/psshop/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse($request, $id);
    }
}
