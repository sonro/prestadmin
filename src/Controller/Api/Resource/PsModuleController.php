<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Entity\PsModule;
use App\Form\PsModuleType;
use App\Supervisor\PsModuleSupervisor;

class PsModuleController extends ApiResourceController
{
    /**
     * @var PsModuleSupervisor
     */
    protected $supervisor;

    public function __construct(
        SerializerInterface $serializer,
        PsModuleSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = PsModule::class;
        $this->entityTypeClass = PsModuleType::class;
        $this->resourceName = 'psModule';
    }

    /**
     * @Route("/api/resource/psmodule", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("/api/resource/psmodule", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request);
    }

    /**
     * @Route("/api/resource/psmodule/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("/api/resource/psmodule/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse(
            $id,
            [$this->supervisor, 'removePsModuleRequests']
        );
    }

    /**
     * @Route("/api/resource/psmodule/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse($request, $id);
    }
}
