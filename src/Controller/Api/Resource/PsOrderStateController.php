<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Entity\PsOrderState;
use App\Form\PsOrderStateType;
use App\Supervisor\PsOrderStateSupervisor;

class PsOrderStateController extends ApiResourceController
{
    /**
     * @var PsOrderStateSupervisor
     */
    protected $supervisor;

    public function __construct(
        SerializerInterface $serializer,
        PsOrderStateSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = PsOrderState::class;
        $this->entityTypeClass = PsOrderStateType::class;
        $this->resourceName = 'psOrderState';
    }

    /**
     * @Route("/api/resource/psorderstate", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("/api/resource/psorderstate", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request);
    }

    /**
     * @Route("/api/resource/psorderstate/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("/api/resource/psorderstate/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("/api/resource/psorderstate/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse($request, $id);
    }
}
