<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Entity\Employee;
use App\Form\EmployeeType;
use App\Supervisor\EmployeeSupervisor;

class EmployeeController extends ApiResourceController
{
    /**
     * @var EmployeeSupervisor
     */
    protected $supervisor;

    private $encoderCallback;

    public function __construct(
        SerializerInterface $serializer,
        EmployeeSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = Employee::class;
        $this->entityTypeClass = EmployeeType::class;
        $this->resourceName = 'employee';

        $this->encoderCallback = function ($employee) use ($supervisor) {
            $supervisor->encodePassword($employee);
        };
    }

    /**
     * @Route("/api/resource/employee", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("/api/resource/employee", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request, $this->encoderCallback);
    }

    /**
     * @Route("/api/resource/employee/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("/api/resource/employee/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("/api/resource/employee/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse($request, $id, null, $this->encoderCallback);
    }
}
