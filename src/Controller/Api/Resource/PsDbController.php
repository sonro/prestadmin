<?php

namespace App\Controller\Api\Resource;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PsDb;
use App\Form\PsDbType;
use App\Supervisor\PsDbSupervisor;

class PsDbController extends ApiResourceController
{
    /**
     * @var PsDbSupervisor
     */
    protected $supervisor;

    public function __construct(
        \JMS\Serializer\SerializerInterface $serializer,
        PsDbSupervisor $supervisor
    ) {
        parent::__construct($serializer);
        $this->supervisor = $supervisor;
        $this->entityClass = PsDb::class;
        $this->entityTypeClass = PsDbType::class;
        $this->resourceName = 'psDb';
    }

    /**
     * @Route("/api/resource/psdb", methods={"GET"})
     */
    public function list(Request $request)
    {
        return $this->listResponse($request);
    }

    /**
     * @Route("/api/resource/psdb", methods={"POST"})
     */
    public function add(Request $request)
    {
        return $this->addResponse($request);
    }

    /**
     * @Route("/api/resource/psdb/{id}", methods={"GET"})
     */
    public function show($id)
    {
        return $this->showResponse($id);
    }

    /**
     * @Route("/api/resource/psdb/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        return $this->deleteResponse($id);
    }

    /**
     * @Route("/api/resource/psdb/{id}", methods={"PUT", "PATCH"})
     */
    public function update(Request $request, $id)
    {
        return $this->updateResponse($request, $id);
    }
}
